const kvA = "kvA";
const kvB = "kvB";
const kvC = "kvC";

var layKhuVuc = function(khuVuc){
    switch(khuVuc){
        case kvA:{
            return 2;
        }
        case kvB:{
            return 1;
        }
        case kvC:{
            return 0.5;
        }
        default:{
            return 0;
        }
    }
}
const doiTuong1 = "doiTuong1";
const doiTuong2 = "doiTuong2";
const doiTuong3 = "doiTuong3";
var layDoiTuong = function(doiTuong){
    switch(doiTuong){
        case doiTuong1:{
            return 2.5;
        }
        case doiTuong2:{
            return 1.5;
        }
        case doiTuong3:{
            return 1;
        }
        default:{
            return 0;
        }
    }
}


document.getElementById("btnDiemChuan").addEventListener('click',function(){
    var thongBao = document.getElementById('thongBao1');
    var ketQua = "";
    var diemChuan = document.getElementById('diemChuan').value*1;
    var diemMon1 = document.getElementById('diem1').value*1;
    var diemMon2 = document.getElementById('diem2').value*1;
    var diemMon3 = document.getElementById('diem3').value*1;
    var khuVucValue = document.getElementById('txtKhuVuc').value;
    var doiTuongValue = document.getElementById('txtDoiTuong').value;
    
    var diemKhuVuc = layKhuVuc(khuVucValue);
    var diemDoiTuong = layDoiTuong(doiTuongValue);
    
    var diemDatDuoc = diemMon1 + diemMon2 + diemMon3 + diemKhuVuc + diemDoiTuong;
    if(diemMon1 > 10 || diemMon2 > 10 || diemMon3 > 10){
        ketQua= `<p>Điểm từ 0-10. Mời Bạn nhập lại</p>`;
    }else if(diemMon1==0 || diemMon2==0 || diemMon3==0){
        ketQua = `<p>Bạn đã rớt và Điểm của bạn là ${diemDatDuoc}</p>` 
    }else if(diemDatDuoc >= diemChuan){
        ketQua = `<p>Bạn đã đậu và Điểm của bạn là ${diemDatDuoc}</p>`;
    }else{
        ketQua = `<p>Bạn đã rớt và Điểm của bạn là ${diemDatDuoc}</p>`
    }
    thongBao.innerHTML = ketQua;
});

// TÍNH TIỀN ĐIỆN 





document.getElementById('btnTienDien').addEventListener('click',function(){
    var tinhTien50kwDau = 500;
    var tinhTien50kwDen100kw = 650;
    var tinhTien100kwDen200kw = 850;
    var tinhTien200kwDen350kw = 1100;
    var tinhTienConLai = 1300;
    
    var thongbao = document.getElementById('thongBao2');
    var ten = document.getElementById('ten').value;
    var kw = document.getElementById('kw').value*1;
    var soTienTra = 0;
    
    if(kw ==0){
        soTienTra  = 0
    }
    else if(kw <=50){
        soTienTra = tinhTien50kwDau*50;
    }else if(kw<=100){
        soTienTra = tinhTien50kwDau*50 + (kw-50)*tinhTien50kwDen100kw;
     console.log((kw-50)*tinhTien50kwDen100kw);
    }else if(kw <= 200){
        soTienTra = tinhTien50kwDau*50 + 50*tinhTien50kwDen100kw + (kw-100)*tinhTien100kwDen200kw;
    }else if(kw <= 350){
        soTienTra= tinhTien50kwDau*50 + 50*tinhTien50kwDen100kw + 100*tinhTien100kwDen200kw + (kw-200)*tinhTien200kwDen350kw;
    }else{
        soTienTra= tinhTien50kwDau*50 + 50*tinhTien50kwDen100kw + 100*tinhTien100kwDen200kw + 150*tinhTien200kwDen350kw + (kw - 350)*tinhTienConLai;
    }
    thongbao.innerHTML = `<div>Họ và tên: ${ten}; Tiền điện: ${soTienTra} VNĐ</div>`;
});